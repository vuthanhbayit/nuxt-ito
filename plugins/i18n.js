import Vue from 'vue';

export default function ({store}) {
    Vue.mixin({
        methods: {
            $i(key) {
                let langs = store.state.init.staticTexts;
                if (!langs) return key;
                let lang = langs.find(i => {
                    return i.key.toLowerCase() == key.toLowerCase()
                })
                if (lang) return lang.value
                return key
            }
        }
    });
}
