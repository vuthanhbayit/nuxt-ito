import {Promise} from "q";

export default function ({$axios, store}) {
    $axios.onRequest(config => {
        if (localStorage.getItem('TOKEN')) {
            config.headers.common['Authorization'] = localStorage.getItem('TOKEN');
        }
        // if (store.state.auth.user && store.state.auth.user.token) {
        //     config.headers.common['token'] = store.state.auth.user.token;
        // }
        if (localStorage.getItem('auth._token.local')) {
            config.headers.common['token'] = localStorage.getItem('auth._token.local').split(' ')[1];
        }
        config.headers.common['secret_key'] = '11nfvsMof10XnUdQEWuxgAZta22';
        config.headers.common['Access-Control-Allow-Origin'] = '*';
    });
    $axios.onResponse(response => {
        if (response.data.success) {
            return Promise.resolve(response)
        }
        return Promise.reject(response.data)
   })
}
