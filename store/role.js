export const state = () => ({})
export const mutations = {}
export const getters = {}
export const actions = {
    async getList() {
        return await this.$axios.$post('/cms/role/list');
    },
    async add({}, opts) {
        return await this.$axios.$post('/cms/role/add', opts);
    },
    async update({}, opts) {
        return await this.$axios.$post('/cms/role/update', opts);
    },
    async remove({}, opts) {
        return await this.$axios.$post('/cms/role/remove', opts);
    },
    async permissionListByRole({}, opts) {
        return await this.$axios.$post('/cms/role/permission-list-by-role', opts);
    },
    async updatePermission({}, opts) {
        return await this.$axios.$post('/cms/role/update-permission', opts);
    },
}
