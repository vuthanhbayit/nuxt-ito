export const state = () => ({})
export const mutations = {}
export const getters = {}
export const actions = {
    async search({}, opts) {
        return await this.$axios.$post('/cms/user/search', opts)
    },
    async add({}, opts) {
        return await this.$axios.$post('/cms/user/add', opts)
    },
    async save({}, opts) {
        return await this.$axios.$post('/cms/user/save', opts)
    },
    async changePassword({}, opts) {
        return await this.$axios.$post('/cms/user/change-password', opts)
    },
    async resetPassword({}, opts) {
        return await this.$axios.$post('/cms/user/reset-password', opts)
    },
    async updatePermission({}, opts) {
        return await this.$axios.$post('/cms/user/update-permission', opts)
    },
    async listPermission({}, opts) {
        return await this.$axios.$post('/cms/user/list-permission', opts)
    },
    async permissionListByUser({}, opts) {
        return await this.$axios.$post('/cms/user/permission-list-by-user', opts)
    },
}
