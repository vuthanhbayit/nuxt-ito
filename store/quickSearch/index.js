export const state = () => ({})
export const mutations = {}
export const getters = {}
export const actions = {
    async search({}, opts) {
        return await this.$axios.$post('/cms/quickSearch/search', opts)
    },
    async suggest({}, opts) {
        return await this.$axios.$post('/cms/quickSearch/suggest', opts)
    },
    async addQuickSearch({}, opts) {
        return await this.$axios.$post('/cms/quickSearch/addQuickSearch', opts)
    },
    async deleteQuickSearch({}, opts) {
        return await this.$axios.$post('/cms/quickSearch/deleteQuickSearch', opts)
    }
}
