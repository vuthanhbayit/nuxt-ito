export const state = () => ({})
export const mutations = {}
export const getters = {}
export const actions = {
    async searchCategory({}, opts) {
        return await this.$axios.$post('/cms/community/searchCategory', opts)
    },
    async detailCategory({}, opts) {
        return await this.$axios.$post('/cms/community/detailCategory', opts)
    },
    async updateCategory({}, opts) {
        return await this.$axios.$post('/cms/community/updateCategory', opts)
    },
    async addCategory({}, opts) {
        return await this.$axios.$post('/cms/community/addCategory', opts)
    },
}
