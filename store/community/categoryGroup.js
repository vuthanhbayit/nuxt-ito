export const state = () => ({})
export const mutations = {}
export const getters = {}
export const actions = {
    async searchCategoryGroup({}, opts) {
        return await this.$axios.$post('/cms/community/searchCategoryGroup', opts)
    },
    async detailCategoryGroup({}, opts) {
        return await this.$axios.$post('/cms/community/detailCategoryGroup', opts)
    },
    async updateCategoryGroup({}, opts) {
        return await this.$axios.$post('/cms/community/updateCategoryGroup', opts)
    },
    async addCategoryGroup({}, opts) {
        return await this.$axios.$post('/cms/community/addCategoryGroup', opts)
    },
}
