export const state = () => ({})
export const mutations = {}
export const getters = {}
export const actions = {
    async search({}, opts) {
        return await this.$axios.$post('/cms/community/search', opts)
    },
    async detail({}, opts) {
        return await this.$axios.$post('/cms/community/detail', opts)
    },
    async update({}, opts) {
        return await this.$axios.$post('/cms/community/update', opts)
    },
    async add({}, opts) {
        return await this.$axios.$post('/cms/community/add', opts)
    },
}
