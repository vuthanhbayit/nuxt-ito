export const state = () => ({})
export const mutations = {}
export const getters = {}
export const actions = {
    async list() {
        return await this.$axios.$post('/cms/language/list');
    },
    async listStatic() {
        return await this.$axios.$post('/cms/language/list_static');
    },
    async saveStatic() {
        return await this.$axios.$post('/cms/language/save_static');
    },
}
