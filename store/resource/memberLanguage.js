export const state = () => ({})
export const mutations = {}
export const getters = {}
export const actions = {
    async searchMemberLanguage({}, opts) {
        return await this.$axios.$post('/cms/resource/searchMemberLanguage', opts)
    },
    async detailMemberLanguage({}, opts) {
        return await this.$axios.$post('/cms/resource/detailMemberLanguage', opts)
    },
    async updateMemberLanguage({}, opts) {
        return await this.$axios.$post('/cms/resource/updateMemberLanguage', opts)
    },
    async addMemberLanguage({}, opts) {
        return await this.$axios.$post('/cms/resource/addMemberLanguage', opts)
    },
}
