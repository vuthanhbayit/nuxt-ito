export const state = () => ({})
export const mutations = {}
export const getters = {}
export const actions = {
    async searchHoliday({}, opts) {
        return await this.$axios.$post('/cms/resource/searchHoliday', opts)
    },
    async detailHoliday({}, opts) {
        return await this.$axios.$post('/cms/resource/detailHoliday', opts)
    },
    async updateHoliday({}, opts) {
        return await this.$axios.$post('/cms/resource/updateHoliday', opts)
    },
    async addHoliday({}, opts) {
        return await this.$axios.$post('/cms/resource/addHoliday', opts)
    },
}
