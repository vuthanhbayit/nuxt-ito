export const state = () => ({})
export const mutations = {}
export const getters = {}
export const actions = {
    async searchJob({}, opts) {
        return await this.$axios.$post('/cms/resource/searchJob', opts)
    },
    async detailJob({}, opts) {
        return await this.$axios.$post('/cms/resource/detailJob', opts)
    },
    async updateJob({}, opts) {
        return await this.$axios.$post('/cms/resource/updateJob', opts)
    },
    async addJob({}, opts) {
        return await this.$axios.$post('/cms/resource/addJob', opts)
    },
}
