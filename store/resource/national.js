export const state = () => ({})
export const mutations = {}
export const getters = {}
export const actions = {
    async searchNational({}, opts) {
        return await this.$axios.$post('/cms/resource/searchNational', opts)
    },
    async detailNational({}, opts) {
        return await this.$axios.$post('/cms/resource/detailNational', opts)
    },
    async updateNational({}, opts) {
        return await this.$axios.$post('/cms/resource/updateNational', opts)
    },
    async addNational({}, opts) {
        return await this.$axios.$post('/cms/resource/addNational', opts)
    },
}
