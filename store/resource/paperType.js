export const state = () => ({})
export const mutations = {}
export const getters = {}
export const actions = {
    async searchPaperType({}, opts) {
        return await this.$axios.$post('/cms/resource/searchPaperType', opts)
    },
    async detailPaperType({}, opts) {
        return await this.$axios.$post('/cms/resource/detailPaperType', opts)
    },
    async updatePaperType({}, opts) {
        return await this.$axios.$post('/cms/resource/updatePaperType', opts)
    },
    async addPaperType({}, opts) {
        return await this.$axios.$post('/cms/resource/addPaperType', opts)
    },
}
