export const state = () => ({})
export const mutations = {}
export const getters = {}
export const actions = {
    async searchReportField({}, opts) {
        return await this.$axios.$post('/cms/resource/searchReportField', opts)
    },
    async detailReportField({}, opts) {
        return await this.$axios.$post('/cms/resource/detailReportField', opts)
    },
    async updateReportField({}, opts) {
        return await this.$axios.$post('/cms/resource/updateReportField', opts)
    },
    async addReportField({}, opts) {
        return await this.$axios.$post('/cms/resource/addReportField', opts)
    },
}
