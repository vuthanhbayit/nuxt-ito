export const state = () => ({})
export const mutations = {}
export const getters = {}
export const actions = {
    async searchAddress({}, opts) {
        return await this.$axios.$post('/cms/resource/searchAddress', opts)
    },
    async detailAddress({}, opts) {
        return await this.$axios.$post('/cms/resource/detailAddress', opts)
    },
    async updateAddress({}, opts) {
        return await this.$axios.$post('/cms/resource/updateAddress', opts)
    },
    async addAddress({}, opts) {
        return await this.$axios.$post('/cms/resource/addAddress', opts)
    },
}
