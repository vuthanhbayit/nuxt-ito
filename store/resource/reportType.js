export const state = () => ({})
export const mutations = {}
export const getters = {}
export const actions = {
    async searchReportType({}, opts) {
        return await this.$axios.$post('/cms/resource/searchReportType', opts)
    },
    async detailReportType({}, opts) {
        return await this.$axios.$post('/cms/resource/detailReportType', opts)
    },
    async updateReportType({}, opts) {
        return await this.$axios.$post('/cms/resource/updateReportType', opts)
    },
    async addReportType({}, opts) {
        return await this.$axios.$post('/cms/resource/addReportType', opts)
    },
}
