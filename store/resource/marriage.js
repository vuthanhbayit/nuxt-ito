export const state = () => ({})
export const mutations = {}
export const getters = {}
export const actions = {
    async searchMarriage({}, opts) {
        return await this.$axios.$post('/cms/resource/searchMarriage', opts)
    },
    async detailMarriage({}, opts) {
        return await this.$axios.$post('/cms/resource/detailMarriage', opts)
    },
    async updateMarriage({}, opts) {
        return await this.$axios.$post('/cms/resource/updateMarriage', opts)
    },
    async addMarriage({}, opts) {
        return await this.$axios.$post('/cms/resource/addMarriage', opts)
    },
}
