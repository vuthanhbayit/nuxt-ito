export const state = () => ({})
export const mutations = {}
export const getters = {}
export const actions = {
    async searchBody({}, opts) {
        return await this.$axios.$post('/cms/resource/searchBody', opts)
    },
    async detailBody({}, opts) {
        return await this.$axios.$post('/cms/resource/detailBody', opts)
    },
    async updateBody({}, opts) {
        return await this.$axios.$post('/cms/resource/updateBody', opts)
    },
    async addBody({}, opts) {
        return await this.$axios.$post('/cms/resource/addBody', opts)
    },
}
