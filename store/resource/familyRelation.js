export const state = () => ({})
export const mutations = {}
export const getters = {}
export const actions = {
    async searchFamilyRelation({}, opts) {
        return await this.$axios.$post('/cms/resource/searchFamilyRelation', opts)
    },
    async detailFamilyRelation({}, opts) {
        return await this.$axios.$post('/cms/resource/detailFamilyRelation', opts)
    },
    async updateFamilyRelation({}, opts) {
        return await this.$axios.$post('/cms/resource/updateFamilyRelation', opts)
    },
    async addFamilyRelation({}, opts) {
        return await this.$axios.$post('/cms/resource/addFamilyRelation', opts)
    },
}
