export const state = () => ({})
export const mutations = {}
export const getters = {}
export const actions = {
    async searchChildrenDesire({}, opts) {
        return await this.$axios.$post('/cms/resource/searchChildrenDesire', opts)
    },
    async detailChildrenDesire({}, opts) {
        return await this.$axios.$post('/cms/resource/detailChildrenDesire', opts)
    },
    async updateChildrenDesire({}, opts) {
        return await this.$axios.$post('/cms/resource/updateChildrenDesire', opts)
    },
    async addChildrenDesire({}, opts) {
        return await this.$axios.$post('/cms/resource/addChildrenDesire', opts)
    },
}
