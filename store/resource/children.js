export const state = () => ({})
export const mutations = {}
export const getters = {}
export const actions = {
    async searchChildren({}, opts) {
        return await this.$axios.$post('/cms/resource/searchChildren', opts)
    },
    async detailChildren({}, opts) {
        return await this.$axios.$post('/cms/resource/detailChildren', opts)
    },
    async updateChildren({}, opts) {
        return await this.$axios.$post('/cms/resource/updateChildren', opts)
    },
    async addChildren({}, opts) {
        return await this.$axios.$post('/cms/resource/addChildren', opts)
    },
}
