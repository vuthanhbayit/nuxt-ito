export const state = () => ({})
export const mutations = {}
export const getters = {}
export const actions = {
    async searchWhenMarry({}, opts) {
        return await this.$axios.$post('/cms/resource/searchWhenMarry', opts)
    },
    async detailWhenMarry({}, opts) {
        return await this.$axios.$post('/cms/resource/detailWhenMarry', opts)
    },
    async updateWhenMarry({}, opts) {
        return await this.$axios.$post('/cms/resource/updateWhenMarry', opts)
    },
    async addWhenMarry({}, opts) {
        return await this.$axios.$post('/cms/resource/addWhenMarry', opts)
    },
}
