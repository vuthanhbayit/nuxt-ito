export const state = () => ({})
export const mutations = {}
export const getters = {}
export const actions = {
    async searchInternationalCode({}, opts) {
        return await this.$axios.$post('/cms/resource/searchInternationalCode', opts)
    },
    async detailInternationalCode({}, opts) {
        return await this.$axios.$post('/cms/resource/detailInternationalCode', opts)
    },
    async updateInternationalCode({}, opts) {
        return await this.$axios.$post('/cms/resource/updateInternationalCode', opts)
    },
    async addInternationalCode({}, opts) {
        return await this.$axios.$post('/cms/resource/addInternationalCode', opts)
    },
}
