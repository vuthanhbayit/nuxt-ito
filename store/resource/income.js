export const state = () => ({})
export const mutations = {}
export const getters = {}
export const actions = {
    async searchIncome({}, opts) {
        return await this.$axios.$post('/cms/resource/searchIncome', opts)
    },
    async detailIncome({}, opts) {
        return await this.$axios.$post('/cms/resource/detailIncome', opts)
    },
    async updateIncome({}, opts) {
        return await this.$axios.$post('/cms/resource/updateIncome', opts)
    },
    async addIncome({}, opts) {
        return await this.$axios.$post('/cms/resource/addIncome', opts)
    },
}
