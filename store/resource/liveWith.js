export const state = () => ({})
export const mutations = {}
export const getters = {}
export const actions = {
    async searchLiveWith({}, opts) {
        return await this.$axios.$post('/cms/resource/searchLiveWith', opts)
    },
    async detailLiveWith({}, opts) {
        return await this.$axios.$post('/cms/resource/detailLiveWith', opts)
    },
    async updateLiveWith({}, opts) {
        return await this.$axios.$post('/cms/resource/updateLiveWith', opts)
    },
    async addLiveWith({}, opts) {
        return await this.$axios.$post('/cms/resource/addLiveWith', opts)
    },
}
