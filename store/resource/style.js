export const state = () => ({})
export const mutations = {}
export const getters = {}
export const actions = {
    async searchStyle({}, opts) {
        return await this.$axios.$post('/cms/resource/searchStyle', opts)
    },
    async detailStyle({}, opts) {
        return await this.$axios.$post('/cms/resource/detailStyle', opts)
    },
    async updateStyle({}, opts) {
        return await this.$axios.$post('/cms/resource/updateStyle', opts)
    },
    async addStyle({}, opts) {
        return await this.$axios.$post('/cms/resource/addStyle', opts)
    },
}
