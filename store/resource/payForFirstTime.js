export const state = () => ({})
export const mutations = {}
export const getters = {}
export const actions = {
    async searchPayForFirstTime({}, opts) {
        return await this.$axios.$post('/cms/resource/searchPayForFirstTime', opts)
    },
    async detailPayForFirstTime({}, opts) {
        return await this.$axios.$post('/cms/resource/detailPayForFirstTime', opts)
    },
    async updatePayForFirstTime({}, opts) {
        return await this.$axios.$post('/cms/resource/updatePayForFirstTime', opts)
    },
    async addPayForFirstTime({}, opts) {
        return await this.$axios.$post('/cms/resource/addPayForFirstTime', opts)
    },
}
