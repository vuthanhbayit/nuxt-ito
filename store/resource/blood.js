export const state = () => ({})
export const mutations = {}
export const getters = {}
export const actions = {
    async searchBlood({}, opts) {
        return await this.$axios.$post('/cms/resource/searchBlood', opts)
    },
    async detailBlood({}, opts) {
        return await this.$axios.$post('/cms/resource/detailBlood', opts)
    },
    async updateBlood({}, opts) {
        return await this.$axios.$post('/cms/resource/updateBlood', opts)
    },
    async addBlood({}, opts) {
        return await this.$axios.$post('/cms/resource/addBlood', opts)
    },
}
