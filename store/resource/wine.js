export const state = () => ({})
export const mutations = {}
export const getters = {}
export const actions = {
    async searchWine({}, opts) {
        return await this.$axios.$post('/cms/resource/searchWine', opts)
    },
    async detailWine({}, opts) {
        return await this.$axios.$post('/cms/resource/detailWine', opts)
    },
    async updateWine({}, opts) {
        return await this.$axios.$post('/cms/resource/updateWine', opts)
    },
    async addWine({}, opts) {
        return await this.$axios.$post('/cms/resource/addWine', opts)
    },
}
