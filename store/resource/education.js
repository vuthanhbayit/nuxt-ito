export const state = () => ({})
export const mutations = {}
export const getters = {}
export const actions = {
    async searchEducation({}, opts) {
        return await this.$axios.$post('/cms/resource/searchEducation', opts)
    },
    async detailEducation({}, opts) {
        return await this.$axios.$post('/cms/resource/detailEducation', opts)
    },
    async updateEducation({}, opts) {
        return await this.$axios.$post('/cms/resource/updateEducation', opts)
    },
    async addEducation({}, opts) {
        return await this.$axios.$post('/cms/resource/addEducation', opts)
    },
}
