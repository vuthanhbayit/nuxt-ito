export const state = () => ({})
export const mutations = {}
export const getters = {}
export const actions = {
    async searchReferenceSource({}, opts) {
        return await this.$axios.$post('/cms/resource/searchReferenceSource', opts)
    },
    async detailReferenceSource({}, opts) {
        return await this.$axios.$post('/cms/resource/detailReferenceSource', opts)
    },
    async updateReferenceSource({}, opts) {
        return await this.$axios.$post('/cms/resource/updateReferenceSource', opts)
    },
    async addReferenceSource({}, opts) {
        return await this.$axios.$post('/cms/resource/addReferenceSource', opts)
    },
}
