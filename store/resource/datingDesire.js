export const state = () => ({})
export const mutations = {}
export const getters = {}
export const actions = {
    async searchDatingDesire({}, opts) {
        return await this.$axios.$post('/cms/resource/searchDatingDesire', opts)
    },
    async detailDatingDesire({}, opts) {
        return await this.$axios.$post('/cms/resource/detailDatingDesire', opts)
    },
    async updateDatingDesire({}, opts) {
        return await this.$axios.$post('/cms/resource/updateDatingDesire', opts)
    },
    async addDatingDesire({}, opts) {
        return await this.$axios.$post('/cms/resource/addDatingDesire', opts)
    },
}
