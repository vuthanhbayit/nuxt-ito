export const state = () => ({})
export const mutations = {}
export const getters = {}
export const actions = {
    async searchCigarette({}, opts) {
        return await this.$axios.$post('/cms/resource/searchCigarette', opts)
    },
    async detailCigarette({}, opts) {
        return await this.$axios.$post('/cms/resource/detailCigarette', opts)
    },
    async updateCigarette({}, opts) {
        return await this.$axios.$post('/cms/resource/updateCigarette', opts)
    },
    async addCigarette({}, opts) {
        return await this.$axios.$post('/cms/resource/addCigarette', opts)
    },
}
