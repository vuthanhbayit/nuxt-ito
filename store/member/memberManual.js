export const state = () => ({})
export const mutations = {}
export const getters = {}
export const actions = {
    async search({}, opts) {
        return await this.$axios.$post('/cms/memberVip/search', opts)
    },
    async detail({}, opts) {
        return await this.$axios.$post('/cms/memberVip/detail', opts)
    },
    async changeStatus({}, opts) {
        return await this.$axios.$post('/cms/memberVip/change-status', opts)
    },
    async resetPassword({}, opts) {
        return await this.$axios.$post('/cms/memberVip/reset-password', opts)
    },
    async changeAuthenStatus({}, opts) {
        return await this.$axios.$post('/cms/memberVip/change-authen-status', opts)
    },
    async changeLikeCount({}, opts) {
        return await this.$axios.$post('/cms/memberVip/change-like-count', opts)
    },
    async getImageAuthen({}, opts) {
        return await this.$axios.$post('/cms/memberVip/get-image-authen', opts)
    },
    async searchMemberAuthen({}, opts) {
        return await this.$axios.$post('/cms/memberVip/search-member-authen', opts)
    },
}
