export const state = () => ({})
export const mutations = {}
export const getters = {}
export const actions = {
    async search({}, opts) {
        return await this.$axios.$post('/cms/member/search', opts)
    },
    async detail({}, opts) {
        return await this.$axios.$post('/cms/member/detail', opts)
    },
    async changeStatus({}, opts) {
        return await this.$axios.$post('/cms/member/change-status', opts)
    },
    async resetPassword({}, opts) {
        return await this.$axios.$post('/cms/member/reset-password', opts)
    },
    async changeAuthenStatus({}, opts) {
        return await this.$axios.$post('/cms/member/change-authen-status', opts)
    },
    async changeLikeCount({}, opts) {
        return await this.$axios.$post('/cms/member/change-like-count', opts)
    },
    async getImageAuthen({}, opts) {
        return await this.$axios.$post('/cms/member/get-image-authen', opts)
    },
    async searchMemberAuthen({}, opts) {
        return await this.$axios.$post('/cms/member/search-member-authen', opts)
    },
}
