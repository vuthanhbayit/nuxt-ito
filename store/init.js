export const state = () => ({
    configs: null,
    languages: null,
    staticTexts: null
})
export const getters = {
    configs: state => state.configs,
    languages: state => state.languages,
    staticTexts: state => state.staticTexts,
}
export const mutations = {
    SET_CONFIGS(state, payload) {
        state.configs = payload
    },
    SET_LANGUAGES(state, payload) {
        state.languages = payload
    },
    SET_STATIC_TEXTS(state, payload) {
        state.staticTexts = payload
    }
}
export const actions = {
    async getInit({commit}) {
        try {
            let promise = await this.$axios.$post('/cms/system/init');
            let {configs, languages, staticTexts} = promise.data;
            staticTexts = staticTexts.map(i => {
                let obj = {
                    key: i.info.languageKey,
                    value: i.langs.jp
                }
                return obj
            })
            commit('SET_CONFIGS', configs);
            commit('SET_LANGUAGES', languages);
            commit('SET_STATIC_TEXTS', staticTexts);
        } catch (e) {
            throw e
        }
    },
}
