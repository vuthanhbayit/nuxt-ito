export const state = () => ({})
export const getters = {}
export const mutations = {}
export const actions = {
    async getCaptcha() {
        return await this.$axios.$post('/cms/user/get_captcha')
    },
    async login({commit}, opts) {
        let promise = await this.$axios.$post('/cms/user/login', opts);
        if (!promise.success) throw promise;
        let {user, allPermission, userPermission} = promise.data;
        await this.$auth.setUserToken(user.token);
        await this.$auth.$storage.setUniversal('user', user);
        await this.$auth.$storage.setUniversal('allPermission', allPermission);
        await this.$auth.$storage.setUniversal('userPermission', userPermission);
        return promise;
    },
    async logout() {
        await this.$auth.$storage.setUniversal('user', null);
        await this.$auth.$storage.setUniversal('allPermission', null);
        await this.$auth.$storage.setUniversal('userPermission', null);
        await this.$auth.logout();
    },
    async fetchUser() {
        let list = ['user', 'userPermission', 'allPermission']
        for (let item of list) {
            let res = await this.$auth.$storage.getLocalStorage(item);
            await this.$auth.$storage.setState(item, res)
        }
    }
};
