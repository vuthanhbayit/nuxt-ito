'use strict';

const CONFIG = {
    version: '1.0.0.0',
    server: {
        host: 'localhost',
        port: process.env.PORT || 3012,
    },
    api: {
        url: 'https://ito-apivip.amela.vn'
    },
    secret_key: '11nfvsMof10XnUdQEWuxgAZta22'
};

module.exports = CONFIG;
