'use strict';

const CONFIG = {
    version: '1.0.0.0',
    server: {
        host: 'localhost',
        port: process.env.PORT || 3012,
        domain: 'http://localhost:3010'
    },
    api: {
        // url: 'http://localhost:3009'
        url: 'https://ito-apivip.amela.vn'
    },
    secret_key: '11nfvsMof10XnUdQEWuxgAZta22'
};

module.exports = CONFIG;
