'use strict';

const Enum = {
    StreamType: {
        AudioOnly: 0,
        VideoOnly: 1,
        AudioAndVideo: 2
    },
    Permission:{
        RoleManagement : 1,
        UserManagement : 2,
        ResourceManagement : 3,
        LanguageManagement : 4,
    }
};

module.exports = Enum;